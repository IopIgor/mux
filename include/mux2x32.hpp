#ifndef mux2x32_HPP
#define mux2x32_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(mux2x32) 
{
  static const unsigned bit_data = 32;

  sc_in<sc_bv<bit_data> > A;
  sc_in<sc_bv<bit_data> > B;
  sc_in<bool> sel;
  sc_out<sc_bv<bit_data> > Y;
  
  SC_CTOR(mux2x32) 
  {
    SC_THREAD(behav);
        sensitive << A << B << sel;
  } 
  
 private:
  void behav();
};

#endif
