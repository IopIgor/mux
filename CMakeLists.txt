cmake_minimum_required (VERSION 2.6)

include_directories ("include")

add_library (mux2x32 SHARED "src/mux2x32.cpp")
add_library (mux2x5 SHARED "src/mux2x5.cpp")

add_executable (test_mux2x32 test/test_mux2x32.cpp)
add_executable (test_mux2x5 test/test_mux2x5.cpp)

target_link_libraries (test_mux2x32 mux2x32 systemc)
target_link_libraries (test_mux2x5 mux2x5 systemc)

enable_testing ()

add_test (mux2x32 test_mux2x32)
add_test (mux2x5 test_mux2x5)
