#include <systemc.h>
#include <iostream>
#include "mux2x32.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(1, SC_NS);
const unsigned ele_vectors = 6;
const unsigned bit_data = 32;
const sc_bv<bit_data> A_vector[ele_vectors] = {10, 5, 0, 8, 7, 25};
const sc_bv<bit_data> B_vector[ele_vectors] = {0, 10, 9, 6, 2, 31};
const sc_bv<bit_data> Y_vector_theor[ele_vectors] = {10, 5, 0, 6, 2, 31};

SC_MODULE(TestBench) 
{
 public:
  sc_signal<sc_bv<bit_data> > A;
  sc_signal<sc_bv<bit_data> > B;
  sc_signal<bool> sel;
  sc_signal<sc_bv<bit_data> > Y;

  mux2x32 mux;

  SC_CTOR(TestBench) : mux("mux")
  {
    SC_THREAD(stimulus_thread);

    mux.A(this->A);
    mux.B(this->B);
    mux.sel(this->sel);
    mux.Y(this->Y);
  }

  bool check() 
  {
    bool error = 0;
    for(unsigned i = 0; i < ele_vectors; i++)
    {
      if(Y_vector_theor[i] != Y_vector_res[i])
      {
        cout << "test failed!!/nTheoric value: " << (Y_vector_theor[i]).to_uint()
             << "/nMy value: " <<  (Y_vector_res[i]).to_uint() << endl << endl;
        error = 1;
      }
    }
    return error;
  }
  
 private:
  sc_bv<bit_data> Y_vector_res[ele_vectors];

  void stimulus_thread() 
  {
    sel.write(0);
    for(unsigned i = 0; i < ele_vectors/2; i++)
    { 
      A.write(A_vector[i]);
      B.write(B_vector[i]);
      wait(wait_time);
      cout << "At time " << sc_time_stamp() << ": mux2x32 selected " 
           << Y.read().to_uint() << endl;
      Y_vector_res[i] = Y.read();
    }
    
    sel.write(1);
    for(unsigned i = ele_vectors/2; i < ele_vectors; i++)
    { 
      A.write(A_vector[i]);
      B.write(B_vector[i]);
      wait(wait_time);
      cout << "At time " << sc_time_stamp() << ": mux2x32 selected " 
           << Y.read().to_uint() << endl;
      Y_vector_res[i] = Y.read();
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.check();
}

