#include <systemc.h>
#include "mux2x32.hpp"

using namespace std;
using namespace sc_core;

void mux2x32::behav()
{
    while(true)
    {
    	wait();
    	if(!sel.read()) Y->write(A->read());
    	else Y->write(B->read());
    }
}
