#include <systemc.h>
#include "mux2x5.hpp"

using namespace std;
using namespace sc_core;

void mux2x5::behav()
{
    while(true)
    {
    	wait();
    	if(!sel.read()) Y->write(A->read());
    	else Y->write(B->read());
    }
}
